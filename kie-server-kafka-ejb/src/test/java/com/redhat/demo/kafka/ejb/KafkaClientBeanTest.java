/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.redhat.demo.kafka.ejb;

import java.io.File;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

import javax.ejb.EJB;

import org.I0Itec.zkclient.ZkClient;
import org.arquillian.tutorial.extension.lifecycle.api.AfterUnDeploy;
import org.arquillian.tutorial.extension.lifecycle.api.BeforeDeploy;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.EnterpriseArchive;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;

import com.redhat.demo.kafka.KafkaClient;
import com.redhat.demo.kafka.TopicConsumer;

import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.server.KafkaConfig;
import kafka.server.KafkaServer;
import kafka.utils.MockTime;
import kafka.utils.TestUtils;
import kafka.utils.Time;
import kafka.utils.ZKStringSerializer$;
import kafka.utils.ZkUtils;
import kafka.zk.EmbeddedZookeeper;


@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class KafkaClientBeanTest {
	
	@Deployment
	public static EnterpriseArchive createDeployment() {
		File[] files = Maven.resolver().loadPomFromFile("pom.xml").importRuntimeAndTestDependencies().resolve()
				.withTransitivity().asFile();
		return ShrinkWrap.create(EnterpriseArchive.class)
				.addAsModule(ShrinkWrap.create(JavaArchive.class)
						.addClass(KafkaClientBean.class)
						.addClass(ExecutorBean.class)
						.addClass(KafkaClientBeanTest.class))
				.addAsLibraries(files);
	}
	
	@ClassRule
	public static TemporaryFolder KAFKA_LOGS = new TemporaryFolder();
	@ClassRule
	public static TemporaryFolder ZK_DATA = new TemporaryFolder();
	
	@EJB(lookup = "java:module/KafkaClientBean!com.redhat.demo.kafka.KafkaClient")
	KafkaClient<String, String> client;
	
	static KafkaServer kafkaServer;
	static EmbeddedZookeeper zkServer;
	 
	@BeforeDeploy
	public static void startKafka(){
		
		File kafkaLogs = KAFKA_LOGS.getRoot();
		
		// setup Zookeeper
        zkServer = new EmbeddedZookeeper();
        String zkConnect = "localhost" + ":" + zkServer.port();
        ZkClient zkClient = new ZkClient(zkConnect, 30000, 30000, ZKStringSerializer$.MODULE$);
        ZkUtils zkUtils = ZkUtils.apply(zkClient, false);

        // setup Broker
        Properties brokerProps = new Properties();
        brokerProps.setProperty("zookeeper.connect", zkConnect);
        brokerProps.setProperty("broker.id", "0");
        brokerProps.setProperty("log.dirs", kafkaLogs.getAbsolutePath());
        brokerProps.setProperty("listeners", "PLAINTEXT://" + "localhost" +":" + "9092");
        KafkaConfig config = new KafkaConfig(brokerProps);
        Time mock = new MockTime();
        kafkaServer = TestUtils.createServer(config, mock);

        // create topic
        AdminUtils.createTopic(zkUtils, "kafka.ejb.client", 1, 1, new Properties(), RackAwareMode.Disabled$.MODULE$);
        AdminUtils.createTopic(zkUtils, "kafka.ejb.client-A", 1, 1, new Properties(), RackAwareMode.Disabled$.MODULE$);
        AdminUtils.createTopic(zkUtils, "kafka.ejb.client-B", 1, 1, new Properties(), RackAwareMode.Disabled$.MODULE$);
	}
	
	@AfterUnDeploy
	public static void stopKafka() throws InterruptedException{
		kafkaServer.shutdown();
		zkServer.shutdown();
	}

	@Test
	public void a_produce_and_consume_messages() throws InterruptedException {
		UUID consumerId = UUID.randomUUID();
		UUID message = UUID.randomUUID();
		
		@SuppressWarnings("unchecked")
		TopicConsumer<String, String> topicConsumer = Mockito.mock(TopicConsumer.class);
		Mockito.when(topicConsumer.getId()).thenReturn(consumerId.toString());
		Mockito.when(topicConsumer.getTopics()).thenReturn(Arrays.asList("kafka.ejb.client"));

		client.registerTopicConsumer(topicConsumer);

		// wait for the consumer to start
		Thread.sleep(1000);
		client.send("kafka.ejb.client", "key", "test-A" + message);
		// give the consumer time to see the event
		Thread.sleep(100);
		
		client.deregisterTopicConsumer(consumerId.toString());

		Mockito.verify(topicConsumer).accept("key", "test-A" + message);
	}

	@Test
	public void b_produce_and_consume_messages_on_two_topics() throws InterruptedException {
		UUID consumerA = UUID.randomUUID();
		UUID consumerB = UUID.randomUUID();
		
		UUID messageA = UUID.randomUUID();
		UUID messageB = UUID.randomUUID();
		
		@SuppressWarnings("unchecked")
		TopicConsumer<String, String> topicConsumerA = Mockito.mock(TopicConsumer.class);
		Mockito.when(topicConsumerA.getId()).thenReturn(consumerA.toString());
		Mockito.when(topicConsumerA.getTopics()).thenReturn(Arrays.asList("kafka.ejb.client-A"));

		@SuppressWarnings("unchecked")
		TopicConsumer<String, String> topicConsumerB = Mockito.mock(TopicConsumer.class);
		Mockito.when(topicConsumerB.getId()).thenReturn(consumerB.toString());
		Mockito.when(topicConsumerB.getTopics()).thenReturn(Arrays.asList("kafka.ejb.client-B"));

		client.registerTopicConsumer(topicConsumerA);

		// wait for the consumer to start
		Thread.sleep(1000);
		client.send("kafka.ejb.client-A", "key", "test-B" + messageA);
		// give the consumer time to see the event
		Thread.sleep(100);

		client.registerTopicConsumer(topicConsumerB);

		// wait for the consumer to start
		Thread.sleep(1000);
		client.send("kafka.ejb.client-A", "key", "test-B" + messageA);
		client.send("kafka.ejb.client-B", "key", "test-B" + messageB);
		// give the consumer time to see the event
		Thread.sleep(100);
		
		client.deregisterTopicConsumer(consumerA.toString());
		client.deregisterTopicConsumer(consumerB.toString());

		Mockito.verify(topicConsumerA, Mockito.times(2)).accept("key", "test-B" + messageA);
		Mockito.verify(topicConsumerB).accept("key", "test-B" + messageB);
	}

	@Test
	public void c_register_and_deregister_consumer() throws InterruptedException {
		UUID consumerId = UUID.randomUUID();
		UUID message = UUID.randomUUID();
		
		@SuppressWarnings("unchecked")
		TopicConsumer<String, String> topicConsumer = Mockito.mock(TopicConsumer.class);
		Mockito.when(topicConsumer.getId()).thenReturn(consumerId.toString());
		Mockito.when(topicConsumer.getTopics()).thenReturn(Arrays.asList("kafka.ejb.client"));

		client.registerTopicConsumer(topicConsumer);

		// wait for the consumer to start
		Thread.sleep(1000);
		client.send("kafka.ejb.client", "key", "test-C" + message);
		// give the consumer time to see the event
		Thread.sleep(100);

		client.deregisterTopicConsumer(consumerId.toString());

		// wait for the consumer to start
		Thread.sleep(1000);
		client.send("kafka.ejb.client", "key", "test-C" + message);
		// give the consumer time to see the event
		Thread.sleep(100);

		Mockito.verify(topicConsumer, Mockito.times(1)).accept("key", "test-C" + message);
	}

}
