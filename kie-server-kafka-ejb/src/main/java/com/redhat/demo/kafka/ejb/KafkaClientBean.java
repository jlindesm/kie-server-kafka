/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.redhat.demo.kafka.ejb;

import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.TopicPartition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Multimaps;
import com.redhat.demo.kafka.KafkaClient;
import com.redhat.demo.kafka.TopicConsumer;

@Singleton
@Startup
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class KafkaClientBean implements KafkaClient<String, String> {

	private static final Logger LOG = LoggerFactory.getLogger(KafkaClientBean.class);

	private static final Properties CONSUMER_CONFIG = new Properties();
	{
		CONSUMER_CONFIG.put("bootstrap.servers", "localhost:9092");
		CONSUMER_CONFIG.put("group.id", "kie.server.kafka");
		CONSUMER_CONFIG.put("enable.auto.commit", "true");
		CONSUMER_CONFIG.put("auto.commit.interval.ms", "1000");
		CONSUMER_CONFIG.put("session.timeout.ms", "30000");
		CONSUMER_CONFIG.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		CONSUMER_CONFIG.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
	}
	private static final Properties PRODUCER_CONFIG = new Properties();
	{
		PRODUCER_CONFIG.put("bootstrap.servers", "localhost:9092");
		PRODUCER_CONFIG.put("acks", "all");
		PRODUCER_CONFIG.put("retries", "0");
		PRODUCER_CONFIG.put("batch.size", "16384");
		PRODUCER_CONFIG.put("linger.ms", "1");
		PRODUCER_CONFIG.put("buffer.memory", "33554432");
		PRODUCER_CONFIG.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		PRODUCER_CONFIG.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
	}

	@EJB
	private ExecutorBean executor;

	private KafkaConsumer<String, String> consumer;
	private KafkaProducer<String, String> producer;

	private ImmutableSetMultimap<String, String> topicMappings = ImmutableSetMultimap.of();
	private Map<String, TopicConsumer<String, String>> consumerIndex = new HashMap<>();

	private Future<Void> poll;

	@PostConstruct
	public void init() {
		consumer = new KafkaConsumer<>(CONSUMER_CONFIG);
		producer = new KafkaProducer<>(PRODUCER_CONFIG);
	}

	@PreDestroy
	public void shutdown() {
		if (poll != null) {
			poll.cancel(true);

			// wait for the current polling cycle to complete
			try {
				poll.get();
			} catch (InterruptedException | ExecutionException e) {
				// unexpected, log
				LOG.error("unexpected error while shuting down Kafka client", e);
			}
		}
		consumer.close();
		producer.close();
	}
	

	@Override
	public void send(String topic, String key, String value) {
			producer.send(new ProducerRecord<>(topic, key, value));
	}

	@Override
	public void registerTopicConsumer(TopicConsumer<String, String> topicConsumer) {
		topicMappings = ImmutableSetMultimap.<String, String>builder().putAll(topicMappings)
				.putAll(topicConsumer.getId(), topicConsumer.getTopics()).build();
		consumerIndex.put(topicConsumer.getId(), topicConsumer);
		cycleConsumer();
	}

	@Override
	public void deregisterTopicConsumer(String consumerId) {
		topicMappings = ImmutableSetMultimap.copyOf(Multimaps.filterKeys(topicMappings, not(equalTo(consumerId))));
		consumerIndex.remove(consumerId);
		cycleConsumer();
	}

	private void cycleConsumer() {
		// kill the polling job
		if (poll != null) {
			poll.cancel(true);

			// wait for the current polling cycle to complete
			try {
				poll.get();
			} catch (InterruptedException | ExecutionException e) {
				LOG.error("unexpected error while reinitializing Kafka client", e);
			}
		}

		// update subscriptions
		consumer.subscribe(topicMappings.values());

		// start the polling job
		poll = executor.execute(new ExecutorBean.Runnable() {

			@Override
			public void doRun(SessionContext context) {

				while (!context.wasCancelCalled()) {
					ConsumerRecords<String, String> records = consumer.poll(500);
					for (TopicPartition partition : records.partitions()) {
						for (ConsumerRecord<String, String> record : records.records(partition)) {
							for (String consumerId : topicMappings.inverse().get(partition.topic())) {
								consumerIndex.get(consumerId).accept(record.key(), record.value());
							}
						}
					}
				}

			}
		});

	}

}