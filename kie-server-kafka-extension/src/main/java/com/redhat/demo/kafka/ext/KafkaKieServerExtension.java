/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.redhat.demo.kafka.ext;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.jbpm.services.api.ProcessService;
import org.kie.server.services.api.KieContainerInstance;
import org.kie.server.services.api.KieServerExtension;
import org.kie.server.services.api.KieServerRegistry;
import org.kie.server.services.api.SupportedTransports;
import org.kie.server.services.impl.KieServerImpl;
import org.kie.server.services.jbpm.JbpmKieServerExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.demo.kafka.KafkaClient;
import com.redhat.demo.kafka.TopicConsumer;

/**
 * Adds capabilities to process Kafka event stream as signals to containers.
 * 
 */
public class KafkaKieServerExtension implements KieServerExtension {

	private static final Logger LOG = LoggerFactory.getLogger(KafkaKieServerExtension.class);

	private static final Boolean disabled = Boolean
			.parseBoolean(System.getProperty("org.kie.server.kafka.ext.disabled", "false"));

	private KafkaClient<String, String> client = null;

	private ProcessService processService = null;

	@SuppressWarnings("unchecked")
	private KafkaClient<String, String> getClient() {
		if (client == null) {
			try {
				client = (KafkaClient<String, String>) new InitialContext()
						.lookup("java:module/KafkaClientBean!com.redhat.demo.kafka.KafkaClient");
			} catch (NamingException e) {
				LOG.error("Could not retrieve KafkaClient EJB", e);
			}
		}

		return client;
	}

	@Override
	public boolean isActive() {
		return disabled == false;
	}

	@Override
	public void init(KieServerImpl kieServer, KieServerRegistry registry) {
		KieServerExtension jbpmExtension = registry.getServerExtension(JbpmKieServerExtension.EXTENSION_NAME);
		if (jbpmExtension == null) {
			// no jbpm services, no kafka support
			LOG.warn("Disabling KafkaKieServerExtension due to missing dependency: {}",
					JbpmKieServerExtension.EXTENSION_NAME);
			return;
		}

		processService = getProcessService(jbpmExtension);

		if (processService == null) {
			// no process service, no kafka support
			LOG.warn("Disabling KafkaKieServerExtension due to missing dependency: {}", ProcessService.class.getName());
			return;
		}
	}

	private ProcessService getProcessService(KieServerExtension jbpmExtension) {
		ProcessService processService = null;
		for (Object service : jbpmExtension.getServices()) {
			if (service != null && ProcessService.class.isAssignableFrom(service.getClass())) {
				processService = (ProcessService) service;
				break;
			}
		}
		return processService;
	}

	public static class SignalCallback implements TopicConsumer<String, String> {

		ProcessService processService;
		String id;
		String[] topics;

		private SignalCallback(ProcessService processService, String containerId, String... topics) {
			this.processService = processService;
			this.id = containerId;
			this.topics = topics;
		}

		@Override
		public String getId() {
			return id;
		}

		@Override
		public List<String> getTopics() {
			return Arrays.asList(topics);
		}

		@Override
		public void accept(String k, String v) {
			String[] vs = v.split(":");
			if (vs.length > 1) {
				processService.signalEvent(id, vs[0], vs[1]);
			} else {
				processService.signalEvent(id, vs[0], null);
			}
		}
	}

	@Override
	public void destroy(KieServerImpl kieServer, KieServerRegistry registry) {
	}

	@Override
	public String getImplementedCapability() {
		return "Kafka";
	}

	@Override
	public List<Object> getServices() {
		return Collections.emptyList();
	}

	@Override
	public String getExtensionName() {
		return "Kafka";
	}

	@Override
	public Integer getStartOrder() {
		return 10;
	}

	@Override
	public List<Object> getAppComponents(SupportedTransports type) {
		return Collections.emptyList();
	}

	@Override
	public <T> T getAppComponents(Class<T> serviceType) {
		return null;
	}

	@Override
	public void createContainer(String id, KieContainerInstance kieContainerInstance, Map<String, Object> parameters) {

		Properties containerProperties = new Properties();
		
		try (InputStream propsFile = kieContainerInstance.getKieContainer().getClassLoader()
				.getResourceAsStream("kafka.properties")) {
			if (propsFile != null) {
				containerProperties.load(propsFile);
			}
		} catch (IOException e) {
			LOG.warn("Could not load kafka.properties while creating container " + id, e);
		}

		String topicList = containerProperties.getProperty("topics");
		if (processService != null && topicList != null) {
			String[] topics = topicList.split(",");
			getClient().registerTopicConsumer(new SignalCallback(processService, id, topics));
		}
	}

	@Override
	public void disposeContainer(String id, KieContainerInstance kieContainerInstance, Map<String, Object> parameters) {
		getClient().deregisterTopicConsumer(id);
	}

}
