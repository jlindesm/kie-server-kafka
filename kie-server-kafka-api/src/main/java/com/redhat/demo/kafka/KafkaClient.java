/*
 * Copyright 2016 Red Hat, Inc. and/or its affiliates.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.redhat.demo.kafka;

/**
 * The KafkaClient interface defines a high-level relationship between consumers
 * of Kafka topics and the underlying Kafka client. Implementations of this
 * interface determine how to manage the connection to Kafka and route messages
 * to TopicConsumers.
 *
 */
public interface KafkaClient<K, V> {

	/**
	 * Register a listener to consume messages from a Kafka topic.
	 * 
	 * @param topicConsumer
	 *            TopicConsumer to register
	 */
	public void registerTopicConsumer(TopicConsumer<K, V> topicConsumer);

	/**
	 * De-register a listener and stop it consuming messages from a Kafka topic.
	 * 
	 * @param consumerId
	 *            String id of the TopicConsumer to deactivate
	 */
	public void deregisterTopicConsumer(String consumerId);

	/**
	 * Send the next message produced by the MessageProducer
	 */
	public void send(String topic, K key, V value);

}
